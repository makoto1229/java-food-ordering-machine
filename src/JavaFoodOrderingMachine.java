import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JavaFoodOrderingMachine {
    private JPanel root;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JLabel label1;
    private JTextField textField1;

    int sum = 0;
    void order(String food){
        String f;
        int tmp;

        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering "+food+"! It will be served as soon as possible."
            );
            f = textPane1.getText();
            textPane1.setText(food+"\n"+f);
            sum = sum+100;
            label1.setText(sum+"yen");


        }
    }


    public JavaFoodOrderingMachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to check out?",
                        "Check out Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you. The total price is "+sum+"yen"
                    );
                    sum = 0;
                    label1.setText(sum+"yen");
                    textPane1.setText("");
                }
            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("JavaFoodOrderingMachine");
        frame.setContentPane(new JavaFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
